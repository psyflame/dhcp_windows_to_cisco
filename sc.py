import xlrd
book = xlrd.open_workbook("dhcp.xls")
print("The number of worksheets is {0}".format(book.nsheets))
print("Worksheet name(s): {0}".format(book.sheet_names()))
sh = book.sheet_by_index(0)
print("name: {0}, rows: {1}, cols: {2}".format(sh.name, sh.nrows, sh.ncols))
print("Cell D30 is {0}".format(sh.cell_value(rowx=29, colx=1)))
for rx in range(sh.nrows):
    #print(sh.row(rx))
    #print(sh.cell(rx,1))
    print("ip dhcp pool {0}".format(sh.cell_value(rx,0)))
    print("host {0} 255.255.255.0".format(sh.cell_value(rx,1)))
    s = sh.cell_value(rx,2)
    s = f"{s[:4]}.{s[4:]}"
    s = f"{s[:9]}.{s[9:]}"
    clientid = f"{s[:14]}.{s[14:]}"
    print("client-identifier {0}".format(clientid))
    print("lease 8")
    print("exit\n")